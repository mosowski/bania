package game {
    import flash.display.Sprite;
    import flash.text.TextField;
    import flash.text.TextFormat;
	/**
     * ...
     * @author mosowski
     */
    public class EndScreen {
        public var gameCtx:GameContext;
        public var sprite:Sprite;
        public var textField:TextField;

        public function EndScreen(_gameCtx:GameContext) {
            gameCtx = _gameCtx;
            sprite = new Sprite();
            textField = new TextField();
            textField.defaultTextFormat = new TextFormat("FontGhostWriter", 20, 0xFFFFFF);
            textField.embedFonts = true;
            textField.x = 50;
            textField.y  = 50;
            textField.width = 500;
            textField.height = 300;
            sprite.addChild(textField);
        }

        public function show():void {
            sprite.graphics.clear();
            sprite.graphics.beginFill(0, 1);
            sprite.graphics.drawRect(0, 0, gameCtx.stage.stageWidth, gameCtx.stage.stageHeight);
            sprite.graphics.endFill();
            textField.text = "Would you handle even more of this?"
                + "\nPoints: " + gameCtx.points
                + "\nDeaths: " + gameCtx.deaths
                + "\n"
                + "\nThanks for playing!";
            gameCtx.stage.addChild(sprite);
        }

    }

}