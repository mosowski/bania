package game {
    import flash.media.Sound;
    import flash.net.URLRequest;
    import flash.utils.Dictionary;
	/**
     * ...
     * @author mosowski
     */
    public class SoundManager {
        public var sounds:Dictionary;

        public function SoundManager() {
            sounds = new Dictionary();
        }

        public function loadSound(name:String):void {
            var sound:Sound = sounds[name];
            if (sound  == null) {
                sound = sounds[name] = new Sound(new URLRequest("static/sounds/" + name + ".mp3"));
            }
        }

        public function playSound(name:String):void {
            var sound:Sound = sounds[name];
            if (sound  == null) {
                sound = sounds[name] = new Sound(new URLRequest("static/sounds/" + name + ".mp3"));
            }
            sound.play();
        }

    }

}