package game.levels {
    import game.GameContext;
    import game.Level;
    import game.MusicId;
	/**
     * ...
     * @author mosowski
     */
    public class Level9 extends LevelConfig{

        public function Level9(_gameCtx:GameContext) {
            super(_gameCtx);
            alphaScale = 10.0;
            thetaScale = 2.0;
            nextLevelConfig = null;
            musicId = MusicId.GOKU;
        }

        override public function init():void {
            level = new Level(gameCtx, 0, 0, 0, gameCtx.ctx.rootNode);
            level.fromObject(gameCtx.ctx.loaderMgr.getLoaderJSON("static/level9.bmap"), gameCtx.tileSet);
            level.layers[0].setScene(gameCtx.layer0);
        }

    }

}