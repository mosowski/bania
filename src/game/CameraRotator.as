package game {
    import flash.display.TriangleCulling;
    import flash.geom.Point;
    import flash.geom.Vector3D;
    import gremlin.core.Context;
    import gremlin.scene.Camera;
    import gremlin.scene.Node;
	/**
     * ...
     * @author mosowski
     */
    public class CameraRotator {
        public var gameCtx:GameContext;
        public var camera:Camera;
        public var node:Node;

        // rotation-around speed
        public var alpha:Number;
        public var beta:Number;
        public var gamma:Number;

        public var alphaValue:Number;
        public var thetaValue:Number;

        public var deadMode:Boolean;

        public function CameraRotator(gameCtx:GameContext, viewWidth:int, viewHeight:int) {
            this.gameCtx = gameCtx;
            camera = new Camera(gameCtx.ctx, viewWidth, viewHeight);
            gameCtx.ctx.addListener(Context.RESIZE, onResize);
            gameCtx.ctx.projectionUtils.makePerspectiveMatrix(camera.projectionMatrix, 0.1, 100.0, 55, gameCtx.stage.stageWidth / gameCtx.stage.stageHeight);

            setAlpha(0.18);
            gamma = 30;
            beta = 13.5;
            thetaValue = 0;
        }

        public function setAlpha(alpha:Number):void {
            this.alpha = alpha;
            resetAlpha();
        }

        public function resetAlpha():void {
            alphaValue = (alpha * gameCtx.time * gameCtx.levelConfig.alphaScale) % (Math.PI * 2);
            if (alphaValue < 0) {
                alphaValue += Math.PI * 2;
            }
        }

        public function tick():void {
            if (deadMode == false) {
                alphaValue = (alphaValue + alpha * gameCtx.timeStep * gameCtx.levelConfig.alphaScale * gameCtx.calmFactor) % (Math.PI * 2);
                if (alphaValue < 0) {
                    alphaValue += Math.PI * 2;
                }
                thetaValue -= gameCtx.levelConfig.thetaScale;
            } else {
                beta += 2.0 * gameCtx.timeStep;
            }


            gameCtx.ctx.projectionUtils.makePerspectiveMatrix(camera.projectionMatrix, 0.1, 100.0, 55, gameCtx.stage.stageWidth / gameCtx.stage.stageHeight);
            camera.projectionMatrix.appendRotation(thetaValue, Vector3D.Z_AXIS);


            var pos:Vector3D = node.getPosition();
            camera.viewMatrix.identity();
            camera.viewMatrix.appendTranslation(-pos.x, -pos.y, -pos.z);
            camera.viewMatrix.appendRotation(-90 + Math.sin(alphaValue) * gamma, Vector3D.X_AXIS);
            camera.viewMatrix.appendRotation(Math.cos(alphaValue) * gamma, Vector3D.Z_AXIS);
            camera.viewMatrix.appendTranslation(0, 0, beta);
            camera.viewMatrix.appendRotation(alphaValue * 180/Math.PI, Vector3D.Z_AXIS);

        }

        public function clip():void {
            var out:Point = new Point();
            for (var i:int = 0; i < gameCtx.level.width; ++i) {
                for (var j:int = 0; j < gameCtx.level.height; ++j) {
                    var tile:Tile = gameCtx.level.layers[0].tiles[i][j];
                    if (tile != null) {
                        camera.getScreenPosition(tile.node.position, out);
                        tile.node.setVisible(out.x > -100 && out.y > -100 && out.x < camera.viewWidth  +100 && out.y < camera.viewHeight + 100);
                    }
                }
            }
        }

        public function onResize(params:Object = null):void {
            gameCtx.ctx.projectionUtils.makePerspectiveMatrix(camera.projectionMatrix, 0.1, 100.0, 55, gameCtx.stage.stageWidth / gameCtx.stage.stageHeight);
        }

        public function destroy():void {
            gameCtx.ctx.removeListener(Context.RESIZE, onResize);
        }
    }


}