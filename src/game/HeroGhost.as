package game {
    import flash.geom.Vector3D;
    import game.pickable.Pickable;
    import gremlin.animation.AnimationState;
    import gremlin.core.Context;
    import gremlin.particles.BillboardParticlesEntity;
    import gremlin.scene.AnimatedEntity;
    import gremlin.scene.ModelEntity;
    import gremlin.scene.Node;
    import gremlin.scene.Scene;
	/**
     * ...
     * @author mosowski
     */
    public class HeroGhost extends Hero {

        public var animatedEntity:AnimatedEntity;
        public var idleAnim:AnimationState;
        public var walkAnim :AnimationState;
        public var attackAnim:AnimationState;
        public var isAttacking:Boolean;
        public var isActive:Boolean;
        public var range:Number;
        public var speedScale:Number;

        public var shoutTip:TipArea;
        public var shoutFlag:Boolean;
        public var previousQuote:int;

        public static const quotes:Array = [
            "Boo.",
            "Boo!",
            "Boo! Boo!",
            "Booooo",
            "Boooo!",
            "Boo.",
            "Boo!",
            "Boo! Boo!",
            "Booooo",
            "Boooo!",
            "Ehh..."
        ];

        public function HeroGhost(gameCtx:GameContext) {
            super(gameCtx);
            entity = new AnimatedEntity(gameCtx.ctx.modelMgr.getModelResource("Ghost"), node);
            animatedEntity = entity as AnimatedEntity;
            idleAnim = animatedEntity.getAnimationState("Idle");
            walkAnim = animatedEntity.getAnimationState("Walk");
            attackAnim = animatedEntity.getAnimationState("Attack");
            animatedEntity.setAnimationState("Walk");
            walkAnim.gotoAndPlay();
            collisionComponent = new CollisionComponent(node);
            collisionComponent.setBounds(entity.modelResource.collisionData.collision2d);
            entity.addToScene(gameCtx.layer0);
            collisionEnabled = false;
            shadow.node.setScale(1.5, 1, 1.5);
            radius = 0.5;
            setSpeedScale(1.0);
            isMortal = false;
            previousQuote = -1;
            shoutTip = new TipArea(gameCtx);
            shoutTip.fitArea = true;
            shoutTip.setSize(150, 150);
            shoutTip.node = new Node();
            shoutTip.node.setPosition(0, 1.5, 0);
            node.addChild(shoutTip.node);
        }

        public function setSpeedScale(scale:Number):void {
            speedScale = scale;
            speed = 0.09 * speedScale;
            walkAnim.fps = 30 * speedScale;
        }

        override public function tick():void {
            super.tick();
            animatedEntity.currentAnimationState.advance(gameCtx.timeStep);

            if (collisionComponent.intersects(gameCtx.hero.collisionComponent) && gameCtx.hero.isDead == false) {
                if (animatedEntity.currentAnimationState != attackAnim) {
                    animatedEntity.setAnimationState("Attack");
                    attackAnim.addSingleTimeListener(AnimationState.ANIMATION_COMPELTED, onAttackCompleted);
                    attackAnim.playOnce();
                    isAttacking = true;
                    moveTargetSet = false;
                    var attackDirection:Vector3D = gameCtx.hero.node.position.subtract(node.position);
                    attackDirection.normalize();
                    rotation = Math.atan2( -attackDirection.x, -attackDirection.z);
                    velocity.setTo(0, 0, 0);
                    gameCtx.ctx.tweener.delayedCall(dealHit, 0.5, this);
                }
            }

            if (isAttacking == false) {
                if (isActive == false) {
                    if (Math.sqrt(Math.pow(gameCtx.hero.node.position.x - node.position.x, 2) + Math.pow(gameCtx.hero.node.position.z - node.position.z, 2)) <= range && gameCtx.hero.isDead == false) {
                        isActive = true;
                    }
                }
                if (isActive == true) {
                    if (shoutFlag == false) {
                        shoutFlag = true;
                        var quoteId:int;
                        do {
                            quoteId = int(Math.random() * quotes.length);
                        } while (quoteId == previousQuote);
                        previousQuote = quoteId;
                        say(quotes[quoteId]);
                    }
                    if (moveTargetSet == false) {
                        animatedEntity.setAnimationState("Walk");
                        walkAnim.gotoAndPlay(1);
                        moveTo(gameCtx.hero.node.position.x + (Math.random()-0.5) * 10, 0, gameCtx.hero.node.position.z + (Math.random()-0.5)*10 );
                    }
                } else {
                    animatedEntity.setAnimationState("Idle");
                    idleAnim.gotoAndPlay(1);
                }
            }
        }

        public function say(text:String):void {
            shoutTip.setText(text);
            shoutTip.show();
            gameCtx.ctx.tweener.delayedCall(function():void {
                shoutTip.hide();
                gameCtx.ctx.tweener.delayedCall(function():void {
                    shoutFlag = false;
                }, 8, this)
            }, 4, this);
        }

        private function dealHit():void {
            if (collisionComponent.intersects(gameCtx.hero.collisionComponent)) {
                gameCtx.soundMgr.playSound("n" + int(Math.random() * 5));
                gameCtx.hero.die();
                say("Buahahaha!");
                isActive = false;
            }
        }

        private function onAttackCompleted(params:Object = null):void {
            isAttacking = false;
            animatedEntity.setAnimationState("Walk");
        }

        override public function destroy():void {
            super.destroy();
            entity.removeFromAllScenes();
            shoutTip.destroy();
            gameCtx.ctx.tweener.killAllTweensOf(this);
        }

        override public function fromObject(object:Object):void {
            range = object.range;
            if ("speed" in object) {
                setSpeedScale(object.speed);
            }
        }
    }

}