Mania! Push your perception to the limit!
=========================================

http://mosowski.github.com/game-off-2012

Instructions:
=============

Control: Arrows

Most of instructions are given at tutorial levels. You command one clone of clockwork demon beholder pushed into another realm. Rule is simple, to escape terrifying dimmension, you have to reach the goal point on each level with at least one beholder, collecting as many points as you can. There are plenty obstacles in the game, static and moving as well, sometimes you will have to push some crates and solve a riddle, but main difficulty in this game is orientation. Screen will rotate and stretch more and more over time. Can you handle this?

Screenshots:
============

![Screenshot1](https://raw.github.com/mosowski/game-off-2012/master/screenshots/level6.png)

Cheatsheet:
===========

- If you really have trouble with some level, you can try passing through it within a debug mode (press D key)
- Grim Reaper behaviour is very simple. You just need to fool him to make him leave your way.
- Some blades and forks can be easily evaded by moving near spikes along with their trajectory.
- Ghosts move to some random point near you, just don't panic when ghost is near you.

Creation notes:
================

Weeks 1 & 2:
Creating rendering engine I've called Gremlin. It is my attempt at creating raw Stage3D based Flash renderer. Although I've created similar stuff before, this time I needed to write whole new engine.

Week 3:
Gameplay programming. I've created map exporter for Blender, tileset assets, textures, collision system for the game and character movement.

Week 4:
Content programming. I've put an enemy to the game, created tutorial and levels. Most of time I was in hurry, so code quality dropped a lot :( Last day was for sounds and some polish.


Copyright
=========

Textures, meshes, animations, scripts and sound effects are totally mine. 

Music comes from embedded YouTube player. It plays some crazy Dvar songs: Taai Lira \ Itiir \ Ariil Laat \ Ud Rah \ Laali \ Iih Rah.
I have them on my YouTube account.


Open Source projects I'm using in this game:
============================================

Blender:
3d modelling, map editing, animation. 


GIMP:
Texture creating and editing.


FlashDevelop:
Great IDE for flash development.


telemetry-utils: https://github.com/adamcath/telemetry-utils
A script which enables Adobe Scout profiling flag.


Adobe AGALMiniAssembler.as
Adobe AGAL assembler to bytecode translator.

